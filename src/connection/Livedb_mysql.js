const request = require('request')

exports.TYPES = {
    Int: 'int',
    VarChar: 'varchar'
}

exports.Livedb = class Livedb {
    constructor(config) {
        this._sql_object = {}

        this._config = {
            server: config.server,
            database: config.database,
            projeto: config.projeto,
            key: config.key
        }

        for (let v in this._config) {
            if (!this._config[v]) {
                throw new Error(`falta parametro ${v}`)
            }
        }
    }

    static gera_key(server, projeto, senha) {
        return new Promise(function (i, o) {
            var options = {
                method: 'POST',
                url: server + '/get_new_key',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: {
                    senha: senha,
                    projeto: projeto
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    o(error)
                } else {
                    if (body.status == 0) {
                        o(body)
                    }
                    i(body)
                }
            });
        })
    }

    sql(query) {
        this._sql_object = {
            parameter: {}
        }
        this._sql_object.query = query
        return this
    }

    parameter(key, type, value) {
        if (!key || typeof value == "undefined" || !type) {
            throw new Error('Problema com os parametros')
        }

        if(type == 'varchar'){
            this._sql_object.parameter[key] = `'${value}'`
        } else if(type == 'int'){
            this._sql_object.parameter[key] = value
        } else {
            throw new Error('Não tem esse tipo')
        }
        return this
    }

    execute() {
        var sql_obj = this._sql_object
        var config = this._config

        return new Promise(function (i, o) {
            var options = {
                method: 'POST',
                url: config.server + '/execute_query',
                headers: {
                    'Content-Type': 'application/json',
                    key: config.key,
                    projeto: config.projeto
                },
                body: {
                    sql: sql_obj,
                    config: config
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    o(error)
                } else {
                    if (body.status == 0) {
                        o(body)
                        return
                    }

                    if(Array.isArray(body.result) && body.result.length <= 2) i(body.result[0])
                    else i(body.result)
                }
            });
        })
    }
}