var Livedb = require('./Livedb').Livedb
var type = require('./Livedb').TYPES

var tp = {}
var connect = {};
module.exports = connect;

connect.new = function () {
  return new Promise(function (_i, _o) {
    //Livedb.gera_key("http://167.99.169.156:3005", "projeto", 'l1v3universyt1')
    Livedb.gera_key("http://138.68.29.250:3005", "projeto", 'l1v3universyt1')
      .then(function (live_key) {
        tp = new Livedb({
          //server: "http://167.99.169.156:3005",
          server: "http://138.68.29.250:3005",
          database: "crm",
          projeto: "projeto",
          key: live_key.key
        })
        console.log('--- Banco Fudido Loaded ---')
        _i()
      }).catch(function (err) {
        throw new Error(err)
      })
  })
}

connect.get_contacts = () => {
  return tp.sql(`
    SELECT TOP 30
      *
    FROM dbo.tb_disparo_areas_zumbi
    WHERE updated IS NULL
    `
  ).execute()
}

connect.get_contact_data = (id_contato) => {
  return tp.sql(`
    SELECT
      a.*,
      b.linkedin
    FROM tb_contato a
    LEFT JOIN tb_redes_sociais b ON a.id_contato = b.id_contato
    WHERE a.id_contato = @id_contato
    `
  ).parameter(
    'id_contato', type.Int, id_contato
  ).execute()
}

connect.setContactUpdate = (id) => {
  return tp.sql(`
    UPDATE tb_disparo_areas_zumbi SET updated = getdate() WHERE id_disparo_areas_zumbi = @id_disparo_areas_zumbi
  `
  ).parameter('id_disparo_areas_zumbi', type.Int, id)
    .execute()
}

connect.updateContact = (contact) => {
  return tp.sql(`
  UPDATE tb_disparo_areas_zumbi
  SET
    updated = getdate(),
    mongo_id = @mongo_id,
    linkedin = @linkedin,
    linkedin_name = @linkedin_name,
    linkedin_location = @linkedin_location,
    linkedin_headline = @linkedin_headline,
    linkedin_distance = @linkedin_distance,
    linkedin_connection = @linkedin_connection,
    linkedin_about = @linkedin_about,
    linkedin_actual_company = @linkedin_actual_company,
    linkedin_education = @linkedin_education,
    linkedin_data_insert = @linkedin_data_insert
  WHERE id_disparo_areas_zumbi = @id_disparo_areas_zumbi
  `
  ).parameter('id_disparo_areas_zumbi', type.Int, contact.id_disparo_areas_zumbi)
    .parameter('mongo_id', type.VarChar, contact.mongo_id)
    .parameter('linkedin', type.VarChar, contact.linkedin)
    .parameter('linkedin_name', type.VarChar, contact.linkedin_name)
    .parameter('linkedin_location', type.VarChar, contact.linkedin_location)
    .parameter('linkedin_headline', type.VarChar, contact.linkedin_headline)
    .parameter('linkedin_distance', type.VarChar, contact.linkedin_distance)
    .parameter('linkedin_connection', type.VarChar, contact.linkedin_connection)
    .parameter('linkedin_about', type.VarChar, contact.linkedin_about)
    .parameter('linkedin_actual_company', type.VarChar, contact.linkedin_actual_company)
    .parameter('linkedin_education', type.VarChar, contact.linkedin_education)
    .parameter('linkedin_data_insert', type.VarChar, contact.linkedin_data_insert)
    .execute()
}

connect.checkPerformance = () => {
  return tp.sql(`
    SELECT
    (SELECT COUNT(*) FROM tb_disparo_areas_zumbi WHERE mongo_id IS NOT NULL) AS total_fielded,
    (SELECT COUNT(*) FROM tb_disparo_areas_zumbi WHERE mongo_id IS NULL) AS total_not_fielded,
    (SELECT COUNT(*) FROM tb_disparo_areas_zumbi) AS total,
    (SELECT COUNT(*) FROM tb_disparo_areas_zumbi WHERE updated IS NULL) AS total_not_updated
  `).execute()
}