const request = require('request')

exports.TYPES = {
    BigInt: 'BigInt',
    Binary: 'Binary',
    Bit: 'Bit',
    Char: 'Char',
    Date: 'Date',
    DateTime2: 'DateTime2',
    DateTime: 'DateTime',
    DateTimeOffset: 'DateTimeOffset',
    Decimal: 'Decimal',
    Float: 'Float',
    Image: 'Image',
    Int: 'Int',
    Money: 'Money',
    NChar: 'NChar',
    NText: 'NText',
    NVarChar: 'NVarChar',
    Null: 'Null',
    Numeric: 'Numeric',
    Real: 'Real',
    SmallDateTime: 'SmallDateTime',
    SmallInt: 'SmallInt',
    SmallMoney: 'SmallMoney',
    TVP: 'TVP',
    Text: 'Text',
    Time: 'Time',
    TinyInt: 'TinyInt',
    UDT: 'UDT',
    UniqueIdentifier: 'UniqueIdentifier',
    VarBinary: 'VarBinary',
    VarChar: 'VarChar',
    Xml: 'Xml'
}

exports.Livedb = class Livedb {
    constructor(config) {
        this._sql_object = {}

        this._config = {
            server: config.server,
            database: config.database,
            projeto: config.projeto,
            key: config.key
        }

        for (let v in this._config) {
            if (!this._config[v]) {
                throw new Error(`falta parametro ${v}`)
            }
        }
    }

    static gera_key(server, projeto, senha) {
        return new Promise(function (i, o) {
            var options = {
                method: 'POST',
                url: server + '/get_new_key',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: {
                    senha: senha,
                    projeto: projeto
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    o(error)
                } else {
                    if (body.status == 0) {
                        o(body)
                    }
                    i(body)
                }
            });
        })
    }

    sql(query) {
        this._sql_object = {
            parameter: []
        }
        this._sql_object.query = query
        return this
    }

    parameter(key, type, value) {
        this._sql_object.parameter.push({
            key: key,
            type: type,
            value: value
        })
        return this
    }

    execute() {
        var sql_obj = this._sql_object
        var config = this._config

        return new Promise(function (i, o) {
            var options = {
                method: 'POST',
                url: config.server + '/execute_query',
                headers: {
                    'Content-Type': 'application/json',
                    key: config.key,
                    projeto: config.projeto
                },
                body: {
                    sql: sql_obj,
                    config: config
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    o(error)
                } else {
                    if (body.status == 0) {
                        o(body)
                        return
                    }
                    i(body.result)
                }
            });
        })
    }
}