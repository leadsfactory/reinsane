var Livedb = require('../connection/Livedb_mysql').Livedb
var type = require('../connection/Livedb_mysql').TYPES

var mysql = {}
var f = {}
module.exports = f

f.new = function () {
    return new Promise(function (_i, _o) {
        Livedb.gera_key("http://138.68.29.250:3005", "projeto", 'l1v3universyt1')
            .then(function (live_key) {
                mysql = new Livedb({
                    server: "http://138.68.29.250:3005",
                    database: "mysql",
                    projeto: "projeto",
                    key: live_key.key
                })
                console.log('--- ONDEMAND LOAD ---')
                _i()
            }).catch(function (err) {
                throw new Error(err)
            })
    })
}


f.get_consulta_dash_pos = function () {
    return mysql.sql('select * from vw_consulta_dash_pos')
        .execute()
}

f.get_contato_cursos_pos = function (id_contato) {
    return mysql.sql(`
    select a.id_ondemand_modulo, a.id_projeto, a.nome_projeto, (a.nota/a.perc_aulas) as media_nota, a.modulo, ((a.perc_aulas/b.qtd_aulas)*100) as perc_aulas from
    (select
    e.id_ondemand_modulo, b.id_projeto, h.nome_projeto, g.modulo, sum(d.nota) as nota, (sum(ifnull(i.assistiu_video, 0))) as perc_aulas
    from
        tb_ondemand_login_projeto a
    join tb_ondemand_projeto_modulo b on
        a.id_projeto = b.id_projeto 
    join tb_login_portal c on 
        a.id_login_portal = c.id_login_portal 
    join tb_ondemand_projeto_modulo e on
        e.id_projeto = b.id_projeto and
        e.id_ondemand_modulo = b.id_ondemand_modulo
    join tb_ondemand_modulo_aula f on
        f.id_ondemand_modulo = b.id_ondemand_modulo
    join tb_ondemand_avaliacao_aula d on 
        d.id_contato = c.id_contato and 
        d.id_ondemand_aula = f.id_ondemand_aula 
    join tb_ondemand_modulo g on 
        g.id_ondemand_modulo = e.id_ondemand_modulo 
    join tb_projeto h on 
        h.id_projeto = a.id_projeto 
    join tb_ondemand_contato_aula i on 
        c.id_contato = i.id_contato and 
        f.id_ondemand_aula = i.id_ondemand_aula 
    where d.id_contato = ${id_contato}
    group by
    e.id_ondemand_modulo, b.id_projeto, g.modulo, i.assistiu_video
    order by 
        a.id_projeto,
        e.id_ondemand_modulo
    ) as a 
    join (select
        a.id_projeto AS id_projeto,
        a.id_ondemand_modulo,
        count(distinct b.id_ondemand_aula) AS qtd_aulas
    from
        ((ondemand.tb_ondemand_projeto_modulo a
    join ondemand.tb_ondemand_modulo_aula b on
        ((b.id_ondemand_modulo = a.id_ondemand_modulo)))
    join ondemand.tb_projeto c on
        ((c.id_projeto = a.id_projeto)))
    where
        ((c.id_tipo_projeto = 31)
        and (c.nome_projeto like '%Pós-graduação%'))
    group by
        a.id_projeto, 
        a.id_ondemand_modulo) b on
    a.id_ondemand_modulo = b.id_ondemand_modulo
    where a.perc_aulas <> 0
    group by a.id_ondemand_modulo, a.id_projeto, a.nome_projeto, a.nota, a.modulo, a.perc_aulas, b.qtd_aulas
    limit 1
    `)
    .execute()
}

f.get_module_info_std_profile = function (data) {
    return mysql.sql(`
    select
    e.id_ondemand_aula,
    a.id_projeto,
    e.assistiu_video,
    DATE_FORMAT(e.atualizacao_aula, '%d/%m/%Y') as data_inicio,
    d.id_ondemand_modulo,
    f.nota,
    f.comentario,
    g.aula,
    h.id_contato
    from
    tb_ondemand_login_projeto a join 
    tb_login_portal h on 
    a.id_login_portal = h.id_login_portal 
    join tb_projeto b on
    a.id_projeto = b.id_projeto
    join tb_ondemand_projeto_modulo c on
    b.id_projeto = c.id_projeto
    join tb_ondemand_modulo_aula d on
    c.id_ondemand_modulo = d.id_ondemand_modulo
    join tb_ondemand_contato_aula e on
    d.id_ondemand_aula = e.id_ondemand_aula and 
    h.id_contato = e.id_contato 
    join tb_ondemand_avaliacao_aula f on
    e.id_ondemand_aula = f.id_ondemand_aula and
    f.id_contato = h.id_contato
    join tb_ondemand_aula g on 
    g.id_ondemand_aula = f.id_ondemand_aula 
    where
    h.id_contato = ${data.id_contato}
    and c.id_ondemand_modulo = ${data.id_modulo}
    and a.id_projeto = ${data.id_projeto}
    and e.assistiu_video = 1
    and f.nota is not null
    group by
    e.id_ondemand_aula,
    a.id_projeto,
    e.assistiu_video,
    e.atualizacao_aula,
    d.id_ondemand_modulo,
    f.nota,
    f.comentario,
    g.aula,
    h.id_contato 
    order by 
    e.id_ondemand_aula
    `)
    .execute()
}

f.cria_login_portal_std_prof = function (data) {
    return mysql.sql(`
	insert into tb_login_portal (id_contato, senha, data_cadastro, data_ultimo_login)
	values (${data.id}, '${data.pass}', now(), null)
    ON DUPLICATE KEY UPDATE id_contato=${data.id};
    `)
    .execute()
}

f.procura_id_portal = function (data) {
    console.log(data)
    return mysql.sql(`
    select * from tb_login_portal where id_contato = ${data.id_contato}
    `)
    .execute()
}

f.insere_curso_ondemand_std_prf = function (data) {
    return mysql.sql(`
    call p_libera_acesso_ondemand_std_prof_teste4 (@_id_login_portal, @_id_projeto, @_trial, @_expira)
    `)
    .parameter('_id_login_portal', type.Int, data.id_login_portal)
    .parameter('_id_projeto', type.Int, data.id_projeto)
    .parameter('_trial', type.Int, data.trial)
    .parameter('_expira', type.VarChar, data.expira)
    .execute()
}

f.edita_validade_curso_std_prof = function (data) {
    return mysql.sql(`
    update tb_ondemand_login_projeto set
    data_validade = ${data.data_validade}
    where id_login_portal = ${data.id_login_portal} and 
    id_login_projeto = ${data.id_login_projeto}
    `)
    .execute()
}

f.busca_modulos_projeto_std_prof = function (id_contato, id_projeto) {
    return mysql.sql(`
    select a.id_ondemand_modulo, a.id_projeto, a.nome_projeto, (a.nota/a.perc_aulas) as media_nota, a.modulo, ((a.perc_aulas/b.qtd_aulas)*100) as perc_aulas from
    (select
    e.id_ondemand_modulo, b.id_projeto, h.nome_projeto, g.modulo, sum(d.nota) as nota, (sum(ifnull(i.assistiu_video, 0))) as perc_aulas
    from
        tb_ondemand_login_projeto a
    join tb_ondemand_projeto_modulo b on
        a.id_projeto = b.id_projeto 
    join tb_login_portal c on 
        a.id_login_portal = c.id_login_portal 
    join tb_ondemand_projeto_modulo e on
        e.id_projeto = b.id_projeto and
        e.id_ondemand_modulo = b.id_ondemand_modulo
    join tb_ondemand_modulo_aula f on
        f.id_ondemand_modulo = b.id_ondemand_modulo
    join tb_ondemand_avaliacao_aula d on 
        d.id_contato = c.id_contato and 
        d.id_ondemand_aula = f.id_ondemand_aula 
    join tb_ondemand_modulo g on 
        g.id_ondemand_modulo = e.id_ondemand_modulo 
    join tb_projeto h on 
        h.id_projeto = a.id_projeto 
    join tb_ondemand_contato_aula i on 
        c.id_contato = i.id_contato and 
        f.id_ondemand_aula = i.id_ondemand_aula 
    where d.id_contato = ${id_contato} and b.id_projeto = ${id_projeto} 
    group by
    e.id_ondemand_modulo, b.id_projeto, g.modulo, i.assistiu_video
    order by 
        a.id_projeto,
        e.id_ondemand_modulo
    ) as a 
    join (select
        a.id_projeto AS id_projeto,
        a.id_ondemand_modulo,
        count(distinct b.id_ondemand_aula) AS qtd_aulas
    from
        ((ondemand.tb_ondemand_projeto_modulo a
    join ondemand.tb_ondemand_modulo_aula b on
        ((b.id_ondemand_modulo = a.id_ondemand_modulo)))
    join ondemand.tb_projeto c on
        ((c.id_projeto = a.id_projeto)))
    where
        ((c.id_tipo_projeto = 31)
        and (c.nome_projeto like '%Pós-graduação%'))
    group by
        a.id_projeto, 
        a.id_ondemand_modulo) b on
    a.id_ondemand_modulo = b.id_ondemand_modulo
    where a.perc_aulas <> 0
    group by a.id_ondemand_modulo, a.id_projeto, a.nome_projeto, a.nota, a.modulo, a.perc_aulas, b.qtd_aulas
    `)
    .execute()
}

f.valida_usuario_ou_cria = function (pessoa, id_contato, id_login_portal, cb) {
    mysql.sql('call sp_ondemand_valida_ou_cria_contato_onflow (@id_contato, @nome, @sobrenome, @email, @id_login_portal)')
        .parameter('id_contato', type.Int, id_contato)
        .parameter('id_login_portal', type.Int, id_login_portal)
        .parameter('nome', type.VarChar, pessoa.nome)
        .parameter('sobrenome', type.VarChar, pessoa.sobrenome)
        .parameter('email', type.VarChar, pessoa.email)

        .execute()
        .then(function (a) {
            cb('ok')
        })
        .catch(function (err) {
            cb('bad')
        })
}

f.libera_curso = function (id_contato, id_projeto, validade, cb) {
    mysql.sql('call sp_ondemand_gravar_login_projeto_val (@idContato, @idProjeto, 0, @dataValidade)')
        .parameter('idContato', type.Int, id_contato)
        .parameter('idProjeto', type.Int, id_projeto)
        .parameter('dataValidade', type.VarChar, validade)
        .execute()
        .then(function (a) {
            cb('ok')
        })
        .catch(function (err) {
            cb('bad')
        })
}

f.import_alunos = function (id_projeto, nome, sobrenome, id_contato_crm) {
    return mysql.sql(`
        call import_aluno (@id_projeto, @nome, @sobrenome, @id_contato_crm)
    `)
    .parameter('id_projeto', type.Int, id_projeto)
    .parameter('nome', type.VarChar, nome)
    .parameter('sobrenome', type.VarChar, sobrenome)
    .parameter('id_contato_crm', type.Int, id_contato_crm)
    .execute()
}

f.deleta_aluno = function(id_contato, id_projeto) {
    return mysql.sql(`
        delete from tb_ondemand_login_projeto
        where id_projeto = @id_projeto 
        and id_login_portal = (
            select id_login_portal 
            from tb_login_portal 
            where id_contato = @id_contato
        )
    `)
    .parameter('id_contato', type.Int, id_contato)
    .parameter('id_projeto', type.Int, id_projeto)
    .execute()
}

f.get_info_aula_studdent_reply = function (id_contato, id_calendario_aula, cb) {
    mysql.sql(`
        select
        c.id_projeto, e.aula, f.id_ondemand_modulo, f.modulo, g.nome_projeto, h.nota nota_anterior
        from
        tb_login_portal a
        join tb_ondemand_login_projeto b on 
        b.id_login_portal = a.id_login_portal
        join tb_ondemand_projeto_modulo c on 
        c.id_projeto = b.id_projeto 
        join tb_ondemand_modulo_aula d on 
        d.id_ondemand_modulo = c.id_ondemand_modulo 
        join tb_ondemand_aula e on 
        e.id_ondemand_aula = d.id_ondemand_aula 
        join tb_ondemand_modulo f on 
        f.id_ondemand_modulo = d.id_ondemand_modulo 
        join tb_projeto g on 
        g.id_projeto = b.id_projeto 
        left join 
        (select * from tb_ondemand_avaliacao_aula where id_contato = @id_contato and id_ondemand_aula < @id_calendario_aula order by id_ondemand_aula desc limit 1) h on 
        h.id_contato = a.id_contato 
        where
        a.id_contato = @id_contato
        and e.id_ondemand_aula = @id_calendario_aula`)
        .parameter('id_contato', type.Int, id_contato)
        .parameter('id_calendario_aula', type.Int, id_calendario_aula)        
        .execute()
        .then(function (a) {
            cb(a)
        })
        .catch(function (err) {            
            cb('bad')
        })
}