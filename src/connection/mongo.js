var mongo_client = require('mongodb').MongoClient

//const url = 'mongodb://donald:corpfebra#2014@200.155.156.186:27017'
const url = 'mongodb://donald:corpfebra#2014@virtual.febracorp.org.br:27017'
//const url = 'mongodb://localhost:27017'

var mongo = {}
var counter = 0
module.exports = mongo


function get_connect(collection, dbName, cb) {
    try{
        const client = new mongo_client(url, { useNewUrlParser: true, useUnifiedTopology: true });
        client.connect(function (err, client) {
            if (err) {
                if (err.name == 'MongoNetworkError' && counter < 2) {
                    counter++
                    setTimeout(() => {
                        get_connect(cb)
                    }, 3000);
                    return
                }
                throw new Error(err)
            }
            counter = 0
            const db = client.db(dbName);
            const col = db.collection(collection);
            cb(col, client)
        })
    } catch(err){
        console.error(err);
    }
}

mongo.connection = get_connect

mongo.find = function(collection = 'teste', dbName = 't1', search = {}, sort = {}, fields = {}, limit = 0) {
    return new Promise(function (p_in, p_out) {
        get_connect(collection, dbName, (db, client) => {
            db.find(search).sort(sort).project(fields).limit(limit).toArray(function (err, r) {
                if (err) {
                    p_out(err)
                    throw new Error(err)
                }
                client.close()
                p_in(r)
                return
            })
        })
    })
}

mongo.insert = function(collection = 'teste', dbName = 't1', document) {
    return new Promise(function (p_in, p_out) {
        get_connect(collection, dbName, (db, client) => {
            db.insertOne(document, function (err, r) {
                if (err) {
                    p_out(err)
                    throw new Error(err)
                }
                client.close()
                p_in(r)
                return
            })
        })
    })
}

mongo.updateOne = function (collection = 'teste', dbName = 't1', obj, set, options = {upsert: false}) {
    return new Promise(function (p_in, p_out) {
        get_connect(collection, dbName, (db, client) => {
            db.updateOne(obj, set, options, function (err, r) {
                if (err) {
                    p_out(err)
                    throw new Error(err)
                }
                client.close()
                p_in(r.result)
                return
            })
        })
    })
}

mongo.deleteOne = function (collection = 'teste', dbName = 't1', obj) {
    return new Promise(function (p_in, p_out) {
        get_connect(collection, dbName, (db, client) => {
            db.deleteOne(obj, function (err, r) {
                if (err) {
                    p_out(err)
                    throw new Error(err)
                }
                client.close()
                p_in(r.result)
            })
        })
    })
}
  
///////////////////////////////////IMPORT DO RATO

mongo.insert_docs = function (obj, collection = 'teste', dbName = 't1') {
    if(!Array.isArray(obj)){
        obj = [obj]
    }
    return new Promise(function (p_in, p_out) {
        get_connect(collection, dbName, (db, client) => {
            db.insertMany(obj, function (err, r) {
                if (err) {
                    p_out(err)
                    throw new Error(err)
                }
                client.close()
                p_in(r)
            })
        })
    })
}

mongo.get_one = function (collection = 'teste', dbName = 't1', search = {}) {
    return new Promise(function (p_in, p_out) {
        get_connect(collection, dbName, (db, client) => {
            db.findOneAndDelete(search, function (err, r) {
                if (err) {
                    p_out(err)
                    throw new Error(err)
                }
                client.close()
                p_in(r.value)
            })
        })
    })
}

mongo.aggregate = function (collection = 'teste', dbName = 't1', search = {}) {
    return new Promise(function (p_in, p_out) {
        get_connect(collection, dbName, (db, client) => {
            db.aggregate(search).toArray(function (err, r) {
                if (err) {
                    p_out(err)
                    throw new Error(err)
                }
                client.close()
                p_in(r)
            })
        })
    })
}

/////////////////////////////////////////////////