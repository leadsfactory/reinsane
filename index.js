const mongo = require('./src/connection/mongo');
const sql = require('./src/connection/crm');

class Reinsane {
  contacts = [];
  status = {
    fielded: 0,
    not_fielded: 0,
    total: 0,
  }

  constructor() {
    this.controller();
  }

  async controller() {
    console.info('\n\nStarting...');

    try {
      this.contacts = await this.getContacts();
      this.status.total = this.contacts.length;
      this.checkEnd();
      this.contacts = await this.getContactInformationSql();
      this.contacts = await this.getContactInformationMongo();
      await this.updateContact();
      await this.setUpdateTime();
      await this.checkPerformance();

      new Reinsane();
    } catch (error) {
      this.showError(error);
    }
  }

  getContacts() {
    console.info('Getting contacts...');
    return sql.get_contacts();
  }

  checkEnd() {
    if (this.contacts.length === 0) {
      console.info('Ending...');
      console.info('Try:', 'SELECT * FROM tb_disparo_areas_zumbi WHERE mongo_id IS NOT NULL ORDER BY updated DESC')
      process.exit();
    }
  }

  async getContactInformationSql() {
    console.info('Getting contact information...');
    const contactsWithLinkedin = [];
    for (const contact of this.contacts) {
      const sqlData = await sql.get_contact_data(contact.id_contato);
      const sqlDataObj = sqlData[0];

      let formattedLinkedin = this.formattingLinkedin(sqlDataObj.linkedin || sqlDataObj.linklinkedin || null);

      formattedLinkedin = formattedLinkedin ? formattedLinkedin.replace(/\?/g, '') : null;

      contactsWithLinkedin.push({
        ...contact,
        formattedLinkedin,
      })
    }

    return contactsWithLinkedin;
  }

  formattingLinkedin(linkedin) {
    if (linkedin && linkedin.length > 0) {
      return (linkedin.indexOf('linkedin.com') > -1) ? linkedin.slice((linkedin.indexOf('/in/') + 4), linkedin.length).replace('/', '') : linkedin;
    }

    return null;
  }

  async getContactInformationMongo() {
    console.info('Checking contact information...');
    const contactsWithMongoData = [];
    for (const contact of this.contacts) {
      let updatedContact = contact;
      if (contact.formattedLinkedin) {
        const search = new RegExp(contact.formattedLinkedin, 'i');

        const mongoData = await mongo.find('linkedin_v1', 'db_new_linkedin', {
          "link": search,
        });

        if (mongoData.length !== 0) {
          const dictionary = [
            ['mongo_id', '_id'],
            ['linkedin', 'link'],
            ['linkedin_name', 'nome'],
            ['linkedin_location', 'location'],
            ['linkedin_headline', 'headline'],
            ['linkedin_distance', 'distance'],
            ['linkedin_connection', 'connection'],
            ['linkedin_about', 'about'],
            ['linkedin_actual_company', 'actual_company'],
            ['linkedin_education', 'education'],
            ['linkedin_data_insert', 'data_insert'],
          ];

          for (const word of dictionary) {
            const types = {
              string: () => contact[word[0]] = mongoData[0][word[1]] && mongoData[0][word[1]] !== '' ? mongoData[0][word[1]] : null,
              object: () => {
                if (Array.isArray(mongoData[0][word[1]])) {
                  return contact[word[0]] = mongoData[0][word[1]][0] && mongoData[0][word[1]][0] !== '' ? mongoData[0][word[1]][0] : null;
                }
                return contact[word[0]] = !!mongoData[0][word[1]] && mongoData[0][word[1]] !== '' ? mongoData[0][word[1]] : null;
              }
            }

            types[typeof (mongoData[0][word[1]])]();
          }

          updatedContact = contact;
        }
      }

      contactsWithMongoData.push(updatedContact);
    }

    return contactsWithMongoData;
  }

  async updateContact() {
    for (const contact of this.contacts) {
      if (contact.mongo_id) {
        console.info('Updating contact...', contact);
        const response = await sql.updateContact(contact);
        if (response && response.length > 0) console.error(response);
        this.status.fielded++;
      } else {
        this.status.not_fielded++;
      }
    }
  }

  async setUpdateTime() {
    console.info('Updating date...');
    for (const contact of this.contacts) {
      const response = await sql.setContactUpdate(contact.id_disparo_areas_zumbi);

      if (response && response.length > 0) console.error(response);
    }
  }

  async checkPerformance() {

    const sqlResult = await sql.checkPerformance();
    console.info('Status:', {
      wave: this.status,
      sql: sqlResult[0],
    });
  }

  showError(error) {
    console.error(error);
  }
}

(function () {
  sql.new().then(() => {
    new Reinsane();
  }).catch(error => {
    console.error('Problema com o sql', error);
  })
})();